from matplotlib import pyplot as plt

import contours
import networks
import caffe
import os
import subprocess

CAFFE_ROOT = '/home/intern/soft/caffe'

# contours.contours_main()  # Gets LV contours and saves 'em into lmdb files
# networks.make_nets()  # Creates protocols for learning

caffe.set_device(0)
caffe.set_mode_gpu()  # or caffe.set_mode_cpu() for machines without a GPU

try:
    del solver  # it is a good idea to delete the solver object to free up memory before instantiating another one
    solver = caffe.SGDSolver('fcn_solver.prototxt')
except NameError:
    solver = caffe.SGDSolver('fcn_solver.prototxt')

# each blob has dimensions batch_size x channel_dim x height x width
print [(k, v.data.shape) for k, v in solver.net.blobs.items()]

# print the layers with learnable weights and their dimensions
print [(k, v[0].data.shape) for k, v in solver.net.params.items()]

# print the biases associated with the weights
print [(k, v[1].data.shape) for k, v in solver.net.params.items()]

# params and diffs have the same dimensions
print [(k, v[0].diff.shape) for k, v in solver.net.params.items()]

# forward pass with randomly initialized weights
solver.net.forward()  # train net
solver.test_nets[0].forward()  # test net (more than one net is supported)

# visualize the image data and its correpsonding label from the train net
img_train = solver.net.blobs['data'].data[0, 0, ...]
plt.imshow(img_train)
plt.show()
label_train = solver.net.blobs['label'].data[0, 0, ...]
plt.imshow(label_train)
plt.show()

# visualize the image data and its correpsonding label from the test net
img_test = solver.test_nets[0].blobs['data'].data[0, 0, ...]
plt.imshow(img_test)
plt.show()
label_test = solver.test_nets[0].blobs['label'].data[0, 0, ...]
plt.imshow(label_test)
plt.show()

# take one step of stochastic gradient descent consisting of both forward pass and backprop
solver.step(1)

# visualize gradients after backprop. If non-zero,
# then gradients are properly propagating and the nets are learning something
# gradients are shown here as 10 x 10 grid of 5 x 5 filters
plt.imshow(
    solver.net.params['conv1'][0].diff[:, 0, ...].reshape(10, 10, 5, 5).transpose(0, 2, 1, 3).reshape(10 * 5, 10 * 5),
    'gray')
plt.show()

ret = subprocess.call(os.path.join(CAFFE_ROOT, 'build/tools/caffe') +
                      ' ' + 'train -solver=fcn_solver.prototxt 2> fcn_train.log', shell=True)